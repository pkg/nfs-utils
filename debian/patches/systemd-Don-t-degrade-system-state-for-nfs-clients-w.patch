From: Joachim Falk <joachim.falk@gmx.de>
Date: Tue, 10 Jan 2023 09:55:59 -0500
Subject: systemd: Don't degrade system state for nfs-clients when krb5 keytab
 present but not containing the nfs/<FQDN> principal
Origin: https://git.linux-nfs.org/?p=steved/nfs-utils.git;a=commit;h=5faf98a62843d1cda2a0fe15e0878fa1281e42b8
Bug-Debian: https://bugs.debian.org/985002

The nfs-client.target requires the auth-rpcgss-module.service, which in
turn requires the rpc-svcgssd.service. However, the rpc.svcgssd daemon
is unnecessary for an NFS client, even when using Kerberos security.
Moreover, starting this daemon with its default configuration will fail
when no nfs/<host>@REALM principal is in the Kerberos keytab. Thus,
resulting in a degraded system state for NFS client configurations
without nfs/<host>@REALM principal in the Kerberos keytab. However, this
is a perfectly valid NFS client configuration as the nfs/<host>@REALM
principal is not required for mounting NFS file systems. This is even
the case when Kerberos security is enabled for the mount!

Installing the gssproxy package hides this problem as this disables the
rpc-svcgssd.service.

Link: http://bugs.debian.org/985002
Link: https://salsa.debian.org/kernel-team/nfs-utils/-/merge_requests/23

Signed-off-by: Joachim Falk <joachim.falk@gmx.de>
Signed-off-by: Steve Dickson <steved@redhat.com>
---
 systemd/auth-rpcgss-module.service | 2 +-
 systemd/nfs-server.service         | 2 +-
 2 files changed, 2 insertions(+), 2 deletions(-)

diff --git a/systemd/auth-rpcgss-module.service b/systemd/auth-rpcgss-module.service
index 25c9de8088fc..4a69a7b77b03 100644
--- a/systemd/auth-rpcgss-module.service
+++ b/systemd/auth-rpcgss-module.service
@@ -8,7 +8,7 @@
 Description=Kernel Module supporting RPCSEC_GSS
 DefaultDependencies=no
 Before=gssproxy.service rpc-svcgssd.service rpc-gssd.service
-Wants=gssproxy.service rpc-svcgssd.service rpc-gssd.service
+Wants=gssproxy.service rpc-gssd.service
 ConditionPathExists=/etc/krb5.keytab
 ConditionVirtualization=!container
 
diff --git a/systemd/nfs-server.service b/systemd/nfs-server.service
index b432f9102d0c..2cdd786841ea 100644
--- a/systemd/nfs-server.service
+++ b/systemd/nfs-server.service
@@ -15,7 +15,7 @@ After=nfsdcld.service
 Before=rpc-statd-notify.service
 
 # GSS services dependencies and ordering
-Wants=auth-rpcgss-module.service
+Wants=auth-rpcgss-module.service rpc-svcgssd.service
 After=rpc-gssd.service gssproxy.service rpc-svcgssd.service
 
 [Service]
-- 
2.39.0

